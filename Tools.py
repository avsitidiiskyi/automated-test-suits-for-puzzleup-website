# Female/Male name depending on random selection
import random
import csv
from string import ascii_letters
from random import choice
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta

from variable_storage import *


def get_word_list_from_file(file_name):
    with open(file_name, "r", encoding='utf-8') as f:
        csv_reader = csv.reader(f)
        ruskie_list = [line[0] for line in csv_reader]
    return ruskie_list
def ruskie_name_male():
    return get_word_list_from_file("ruskie_name_male.csv")
def ruskie_name_FEmale():
    return get_word_list_from_file("ruskie_name_FEmale.csv")
def ruskie_surname_male():
    return get_word_list_from_file("ruskie_surname_male.csv")
def ruskie_surname_FEmale():
    return get_word_list_from_file("ruskie_surname_FEmale.csv")
def get_random_male_name():
    return random.choice(ruskie_name_male())
def get_random_FEmale_name():
    return random.choice(ruskie_name_FEmale())
def get_random_male_surname():
    return random.choice(ruskie_surname_male())
def get_random_FEmale_surname():
    return random.choice(ruskie_surname_FEmale())
def get_random_name():
    return random.choice([get_random_male_name(), get_random_FEmale_name()])
def get_random_surname_for_given_name(name):
    if name in ruskie_name_male():
        return get_random_male_surname()
    return get_random_FEmale_surname()


def magic():
    random_name = get_random_name()
    random_surname = get_random_surname_for_given_name(random_name)
    return random_name, random_surname


name, surname = magic()


def name_for_email():
    with open("names.csv", "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        name_list = []
        for line in csv_reader:
            name_list.append(line[0])

    final_name = random.choice(name_list)
    return final_name


def e_mail():
    first_email_part = (''.join(choice(ascii_letters) for i in range(4)))
    second_email_part = name_for_email()
    final_part = random.choice(domains)
    result = str(first_email_part) + str(second_email_part) + str(final_part)
    return result


# _________________Phone_function_________________
def phone():
    var_code = [96, 50, 97, 98, 73, 99]
    code = random.choice(var_code)
    num = (random.randrange(9000000))
    result = str(code) + str(num)
    return result


def password():
    a = (''.join(choice(ascii_letters) for i in range(7)))
    num = (random.randrange(900))

    spec = (random.choice(special))
    return a + str(spec) + str(num)


def password_writer(file):
    with open("password.txt", 'w') as writeable:
        writeable.write(file)


def email_writer(file):
    with open("e-mail.txt", 'w') as writeable:
        writeable.write(file)


def nickname_writer(file):
    with open("nickname.txt", 'w') as writeable:
        writeable.write(file)


def reader(readable_file):
    with open(readable_file) as readable:
        read = readable.read()
        return read


def name_surname_writer(file):
    with open("name and surname.txt", "a") as writable:
        writable.write(file)


def get_city():
    with open("cities.csv", "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        city_list = [line for line in csv_reader]
    city = random.choice(city_list)
    return city[0]


# date from current year
def get_random_date():
    start_date = date.today().replace(day=1, month=1).toordinal()
    end_date = date.today().toordinal()
    random_day = date.fromordinal(random.randint(start_date, end_date))
    return random_day


def years_ago(years):
    old_date = datetime.now()
    new_date = str(old_date - relativedelta(years=years))
    return new_date[:10]


# for boundary values
def years_ago_plus_day(years):
    old_date = datetime.now()
    new_date = old_date + relativedelta(days=1)
    result = str(new_date - relativedelta(years=years))

    return result[:10]


def next_day():
    current_date = datetime.now()
    nxt_day = str(current_date + relativedelta(days=1))
    return nxt_day[:10]

