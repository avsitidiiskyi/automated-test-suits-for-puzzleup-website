from selenium import webdriver
from selenium.webdriver.common.by import By


class LocatorsForRegistration:
    main = (By.XPATH, "//a[contains(text(),'Register')]")
    register_from_login = (By.XPATH, "//a[contains(text(),'Register Now')]")
    go_crush_it = (By.XPATH, "//a[contains(text(),'Go crush it')]")
    create_an_account = (By.XPATH, "//a[@class='main_button']")
    join_now = (By.XPATH, "//a[@class='button2']")
    main_page_register_now = (By.XPATH, "//span[contains(text(),'Register now')]")
    main_page_return = (By.XPATH, "//a[contains(text(),'Main Page')]")

    our_rules = (By.XPATH, "//a[contains(text(),'Our Rules')]")

    log_in = (By.XPATH, "//div[@class='puzzleup_header_content']//a[contains(text(),'Log In')]")


class LocatorsForLogin:
    main = (By.XPATH, "//div[@class='puzzleup_header_content']//a[contains(text(),'Log In')]")
    login_main_bottom = (By.XPATH, "//a[@class='button1']")
    have_acc = (By.XPATH, "//p[@class='have_ac']//a[contains(text(),'Log In')]")
    reg_btn = (By.XPATH, "//div[@class='puzzleup_header_content']//a[contains(text(),'Register')]")
    main_page_return = (By.XPATH, "//a[contains(text(),'Main Page')]")


class UserRegistration:
    username = (By.XPATH, "//input[@id='id_username']")
    first_name = (By.XPATH, "//input[@id='id_first_name']")
    last_name = (By.XPATH, "//input[@id='id_last_name']")
    email = (By.XPATH, "//input[@id='id_email']")
    password = (By.XPATH, "//input[@id='id_password1']")
    password_confirmation = (By.XPATH, "//input[@id='id_password2']")
    city = (By.XPATH, "//input[@id='id_city']")
    choose_photo = (By.XPATH, "//label[@class='btn_form']")
    hobby = (By.XPATH, "//textarea[@id='id_hobby']")
    submit = (By.XPATH, "//input[@class='btn_form']")
    dob = (By.XPATH, "//input[@id='id_date_of_birth']")
    registration_message = (By.XPATH, "//p[@class='registration_message']")


class Auth:
    login = (By.XPATH, "//input[@placeholder='Username/ Mail']")
    password = (By.XPATH, "//input[@placeholder='Password']")
    login_btn = (By.XPATH, "//input[@class='btn_form']")


class UserAccount(Auth):
    log_out = (By.XPATH, "//a[contains(text(),'Log Out')]")
    edit_profile = (By.XPATH, "//a[contains(text(),'Edit Profile')]")
    add_photo = (By.XPATH, "//a[contains(text(),'Add Photos')]")
    find_date = (By.XPATH, "//a[contains(text(),'Find a Date')]")

    class ProfileEdit:
        first_name = (By.XPATH, "//input[@id='id_first_name']")
        last_name = (By.XPATH, "//input[@id='id_last_name']")
        city = (By.XPATH,  "//input[@id='id_city']")
        dob = (By.XPATH, "//input[@id='id_date_of_birth']")
        choose_photo = (By.XPATH, "//label[@class='btn_form']")
        hobby = (By.XPATH, "//textarea[@id='id_hobby']")
        submit = (By.XPATH, "//input[@class='btn_form']")
        profile_image = (By.XPATH, "//img[@class='user_profile__img']")

    class Password:
        change_password_btn = (By.XPATH, "//a[@class='change_password_link']")
        old_password_field = (By.XPATH, "//input[@id='id_old_password']")
        new_password_filed = (By.XPATH, "//input[@id='id_new_password1']")
        confirmation_password_field = (By.XPATH, "//input[@id='id_new_password2']")
        submit = (By.XPATH, "//input[@class='btn_form']")

    class Photos:
        add_photo_btn = (By.XPATH, "//a[contains(text(),'Add Photos')]")
        choose_photo = (By.XPATH, "//label[@class='btn_form']")
        img_desc_field = (By.XPATH, "//textarea[@id='id_image_description']")
        submit = (By.XPATH, "//input[@class='btn_form']")
        first_photo_file = (By.XPATH, "//img[@class='user_photos_list']")
        # after second photo added:
        first_pic = (By.XPATH, "//ul[1]//li[1]//a[1]//img[1]")
        second_pic = (By.XPATH, "//ul[2]//li[1]//a[1]//img[1]")
        cancel = (By.XPATH, "//a[contains(text(),'Cancel')]")
        delete = (By.XPATH, "//input[@class='delete_form']")

    class Date(UserRegistration):
        find_a_date = (By.XPATH, "//a[contains(text(),'Find a Date')]")
        main_page = (By.XPATH, "//a[contains(text(),'Main Page')]")
        login = (By.XPATH, "//div[@class='puzzleup_header_content']//a[contains(text(),'Log In')]")

        nickname = (By.XPATH,"//input[@id='id_username']")
        first_name = (By.XPATH, "//input[@id='id_first_name']")
        last_name = (By.XPATH, "//input[@id='id_last_name']")
        city = (By.XPATH, "//input[@id='id_city']")
        dob = (By.XPATH, "//input[@id='id_date_of_birth']")
        hobby = (By.XPATH, "//input[@id='id_hobby']")

        search = (By.XPATH, "//input[@class='find_date_btn']")
        clear = (By.XPATH, "//a[@class='reset_date_btn']")
























































