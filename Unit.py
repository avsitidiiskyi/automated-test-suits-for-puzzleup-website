import unittest
import requests
from urls import StatusCodes


class SimpleTestExample(unittest.TestCase):
    def test_status_codes(self):
        response = requests.get(StatusCodes.main)
        self.assertEqual(response.ok, True)

    def test_status_codes1(self):
        response = requests.get(StatusCodes.registration)
        self.assertEqual(response.ok, True)

    def test_status_codes2(self):
        response = requests.get(StatusCodes.login)
        self.assertEqual(response.ok, True)

    def test_status_codes3(self):
        response = requests.get(StatusCodes.rules)
        self.assertEqual(response.ok, True)

    def test_status_codes4(self):
        response = requests.get(StatusCodes.contact_us)
        self.assertEqual(response.ok, True)


if __name__ == '__main__':
    unittest.main()
