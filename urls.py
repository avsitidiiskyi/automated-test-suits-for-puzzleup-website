class Urls:
    main_page = "http://45.94.157.160:8111/"
    registration_page = "http://45.94.157.160:8111/registration/"
    authorization_link = "http://45.94.157.160:8111/login/"
    user_page = "http://45.94.157.160:8111/user-page/"


class StatusCodes:
    main = "http://45.94.157.160:8111/"
    registration = "http://45.94.157.160:8111/registration/"
    login = "http://45.94.157.160:8111/login/"
    rules = "http://45.94.157.160:8111/our-rules/"
    contact_us = "http://45.94.157.160:8111/contact-us/"

