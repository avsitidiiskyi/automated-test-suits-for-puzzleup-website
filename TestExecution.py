from Initilization import chrome_setup
from urls import Urls
from Methods import Methods
from Locators import LocatorsForRegistration, LocatorsForLogin, UserRegistration, Auth, UserAccount
from time import sleep
from Tools import *
import pyautogui as gui
from datetime import datetime


class ValidRegistrationLinks(LocatorsForRegistration,  Methods, Urls):
    @staticmethod
    def start(browser):
        action = Methods(browser)
        action.open_url(Urls.main_page)
        sleep(2)
        action.clicker(LocatorsForRegistration.main)
        sleep(2)
        validation_1 = action.get_current_url()
        try:
            assert validation_1 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on Main page is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on Main page is {GREEN}VALID")
        sleep(0.5)

        action.clicker(LocatorsForRegistration.main_page_return)
        sleep(2)
        action.clicker(LocatorsForRegistration.join_now)
        sleep(2)
        validation_2 = action.get_current_url()
        try:
            assert validation_2 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on Main page('Join Now') is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on Main page('Join Now') is {GREEN}VALID")
        sleep(1)

        action.clicker(LocatorsForRegistration.main_page_return)
        sleep(2)
        action.clicker(LocatorsForRegistration.create_an_account)
        sleep(2)
        validation_3 = action.get_current_url()
        try:
            assert validation_3 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on Main page('Create an account') is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on Main page('Create an account') is {GREEN}VALID")

        sleep(1)

        action.clicker(LocatorsForRegistration.main_page_return)
        sleep(2)
        action.clicker(LocatorsForRegistration.main_page_register_now)
        sleep(2)
        validation_4 = action.get_current_url()
        try:
            assert validation_4 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on Main page('Register Now') is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on Main page('Register Now') is {GREEN}VALID")
        sleep(1)

        action.clicker(LocatorsForRegistration.log_in)
        sleep(2)
        action.clicker(LocatorsForRegistration.register_from_login)
        sleep(2)
        validation_5 = action.get_current_url()
        try:
            assert validation_5 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on 'Log in' inlay is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on 'Log in' inlay is {GREEN}VALID")
        sleep(1)

        action.clicker(LocatorsForRegistration.our_rules)
        sleep(2)
        action.clicker(LocatorsForRegistration.go_crush_it)
        sleep(2)
        validation_6 = action.get_current_url()
        try:
            assert validation_6 == correct_url
        except AssertionError:
            print(f"{WHITE}Registration link on 'Our rules' inlay is {RED}BROKEN")
        else:
            print(f"{WHITE}Registration link on 'Our rules' inlay is {GREEN}VALID")
        sleep(1)

        driver.quit()


class ValidLoginLinks(LocatorsForLogin, Methods, Urls):

    @staticmethod
    def start(browser):
        action = Methods(browser)
        action.open_url(Urls.main_page)
        sleep(2)
        action.clicker(LocatorsForLogin.main)
        sleep(1)
        validation_1 = action.get_current_url()
        try:
            assert validation_1 == correct_login_url
        except AssertionError:
            print(f"{WHITE} Login link from main page is {RED}BROKEN")
        else:
            print(f"{WHITE} Login link from main page is {GREEN}VALID")

        sleep(1)
        action.clicker(LocatorsForLogin.main_page_return)
        sleep(1)
        action.clicker(LocatorsForLogin.login_main_bottom)
        sleep(1)
        validation_2 = action.get_current_url()
        try:
            assert validation_2 == correct_login_url
        except AssertionError:
            print(f"{WHITE} Login link from main page(bottom) is {RED}BROKEN")
        else:
            print(f"{WHITE} Login link from main page(bottom) is {GREEN}VALID")
        sleep(1)

        action.clicker(LocatorsForLogin.main_page_return)
        sleep(1)
        action.clicker(LocatorsForLogin.reg_btn)
        sleep(1.5)
        action.clicker(LocatorsForLogin.have_acc)
        sleep(1)
        validation_3 = action.get_current_url()
        try:
            assert validation_3 == correct_login_url
        except AssertionError:
            print(f"{WHITE} Login link from registration page is {RED}BROKEN")
        else:
            print(f"{WHITE} Login link from registration page is {GREEN}VALID")

        driver.quit()


class PositiveRegistration(Methods, Urls, UserRegistration):

    @staticmethod
    def start(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)

        action.typewrite(name_for_email(), UserRegistration.username)
        sleep(2)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite(e_mail(), UserRegistration.email)
        city = ''.join([town for town in get_city()])
        action.typewrite(city, UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(years_ago(18), UserRegistration.dob)
        sleep(1)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        sleep(3)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)
        redirected = action.get_current_url()

        assert redirected == correct_login_url, 'OOPS, looks like registration is not completed'
        print(f'{WHITE}PositiveRegistration {GREEN}PASS')
        driver.quit()


class NegativeRegistration(Methods, Urls, UserRegistration):

    @staticmethod
    def email_is_not_unique(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)
        action.typewrite(name_for_email(), UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite("test@mail.net", UserRegistration.email)
        action.typewrite(get_city(), UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(years_ago(random.randrange(20, 40)), UserRegistration.dob)
        sleep(3)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)
        assert "This email is not unique!" in driver.page_source, "'This email is not unique!' Text is not located"
        print(f"{WHITE}email_is_not_unique {GREEN}PASS")
        driver.quit()

    @staticmethod
    def username_taken(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)
        action.typewrite("test_account", UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite(e_mail(), UserRegistration.email)
        action.typewrite(get_city(), UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(years_ago(random.randrange(20, 40)), UserRegistration.dob)
        sleep(1)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)
        assert "This username is already taken!" in driver.page_source, "'This username is already taken!' " \
                                                                        "Text is not located"
        print(f"{WHITE}username_taken {GREEN}PASS")
        driver.quit()

    @staticmethod
    def both_name_and_email_taken(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)
        action.typewrite("test_account", UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite("test@mail.net", UserRegistration.email)
        action.typewrite(get_city(), UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(years_ago(random.randrange(20, 40)), UserRegistration.dob)
        sleep(1)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)

        assert "This username is already taken!" in driver.page_source,\
            f"{RED}This username is already taken! {WHITE}Text is not located"
        assert "This email is not unique!" in driver.page_source,\
            f"{RED}This email is not unique! {WHITE}Text is not located"
        print(f"{GREEN}Both {WHITE}'This username is already taken!' {GREEN}and{WHITE} "
              f"'This email is not unique!'{GREEN} "
              f"Dynamically displayed messages are located")
        driver.quit()

    @staticmethod
    def future(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)

        action.typewrite(name_for_email(), UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite(e_mail(), UserRegistration.email)
        action.typewrite(get_city(), UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(next_day(), UserRegistration.dob)
        sleep(1)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)
        redirected = action.get_current_url()
        try:
            assert "You can't be born in the future" in driver.page_source
        except AssertionError:
            print(f"{WHITE}'You can't be born in the future' text is {RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'You can't be born in the future' text is {GREEN}LOCATED ")

        assert redirected != correct_login_url
        driver.quit()

    @staticmethod
    def underage(browser):
        action = Methods(browser)
        action.open_url(Urls.registration_page)

        action.typewrite(name_for_email(), UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(name, UserRegistration.first_name)
        action.typewrite(surname, UserRegistration.last_name)
        action.typewrite(e_mail(), UserRegistration.email)
        action.typewrite(get_city(), UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(years_ago_plus_day(18), UserRegistration.dob)
        sleep(2)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(1)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("This is a test text", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(2)
        redirected = action.get_current_url()
        assert redirected == correct_login_url, f'INvalid "UNDERAGE" test {GREEN}PASS'
        print(f'{RED}FAIL!{WHITE} OOPS, looks like I managed to register an account with '
              f'{RED}Underage{WHITE} Date of Birth')
        driver.quit()


class Authorization(Methods, Urls, Auth):
    @staticmethod
    def authorization_by_login(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)

        action.typewrite("_tester_", Auth.login)
        action.typewrite("test_password", Auth.password)
        action.clicker(Auth.login_btn)
        sleep(2)
        assert "John" in driver.page_source, "John Doe is not located"
        print(f"{WHITE}Positive authorization test BY LOGIN {GREEN}PASS")
        driver.quit()

    @staticmethod
    def authorization_by_email(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)

        action.typewrite("JDM@mail.com", Auth.login)
        action.typewrite("test_password", Auth.password)
        action.clicker(Auth.login_btn)
        sleep(2)
        assert "John" in driver.page_source, "John Doe is not located"
        print(f"{WHITE}Positive authorization test BY EMAIL {GREEN}PASS")
        driver.quit()

    @staticmethod
    def double_authorization(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("JDM@mail.com", Auth.login)
        action.typewrite("test_password", Auth.password)
        action.clicker(Auth.login_btn)
        sleep(2)
        action.open_new_url(Urls.authorization_link)
        action.switch_to_window(1)
        action.open_url(Urls.authorization_link)
        action.typewrite("tester_2", Auth.login)
        action.typewrite("test_password", Auth.password)
        action.clicker(Auth.login_btn)
        sleep(2)

        if "Jane" in driver.page_source:
            print(f"{WHITE}Double authorization test {RED}FAIL!{WHITE} Looks like it is possible to authorize user "
                  f"without breaking previous authentication{RED}!")
        else:
            print(f"{WHITE}Double authorization test {GREEN}PASS")

        driver.quit()

    @staticmethod
    def invalid_auth_by_email(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("JDM@mail.com", Auth.login)
        action.typewrite("123qwee", Auth.password)
        action.clicker(Auth.login_btn)

        try:
            assert "Please enter a correct username and password." in driver.page_source
        except AssertionError:
            print(f"{WHITE} 'Please enter a correct username and password' message is {RED}NOT LOCATED")
        else:
            print(f"{WHITE} 'Please enter a correct username and password' message is {GREEN}LOCATED")

        driver.quit()

    @staticmethod
    def invalid_auth_by_login(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("tester_1", Auth.login)
        action.typewrite("123qwee", Auth.password)
        action.clicker(Auth.login_btn)

        try:
            assert "Please enter a correct username and password." in driver.page_source
        except AssertionError:
            print(f"{WHITE} 'Please enter a correct username and password' message is {RED}NOT LOCATED")
        else:
            print(f"{WHITE} 'Please enter a correct username and password' message is {GREEN}LOCATED")

        driver.quit()


class UserPage(Methods, Urls, UserAccount):

    @staticmethod
    def logging_out(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("JDM@mail.com", UserAccount.login)
        action.typewrite("test_password", UserAccount.password)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        try:
            assert "John" in driver.page_source
        except AssertionError:
            print("No John Located")
        action.clicker(UserAccount.log_out)
        sleep(2)
        action.open_new_url(user_page)
        action.switch_to_window(1)
        sleep(3)
        current_url = action.get_current_url()
        try:
            assert current_url != Urls.user_page
        except AssertionError:
            print(f"{WHITE}logging_out FAIL!{RED}Not equal")
        else:
            print(f"{WHITE}Logging out test {GREEN}PASS")
        driver.quit()

    @staticmethod
    def editing(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("x-test", UserAccount.login)
        action.typewrite("typical_password", UserAccount.password)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        action.clicker(UserAccount.edit_profile)
        sleep(2)

        action.clear_field(UserAccount.ProfileEdit.first_name)
        action.clear_field(UserAccount.ProfileEdit.last_name)
        action.clear_field(UserAccount.ProfileEdit.city)
        action.clear_field(UserAccount.ProfileEdit.hobby)
        action.clear_field(UserAccount.ProfileEdit.dob)
        sleep(0.5)

        f_name = [x for x in ''.join(name)]
        s_name = [y for y in ''.join(surname)]

        action.typewrite(f_name, UserAccount.ProfileEdit.first_name)
        action.typewrite(s_name, UserAccount.ProfileEdit.last_name)
        action.typewrite("changed hobby", UserAccount.ProfileEdit.hobby)
        action.typewrite("1999-01-01", UserAccount.ProfileEdit.dob)
        action.typewrite("city-17", UserAccount.ProfileEdit.city)
        sleep(1)
        action.clicker(UserAccount.ProfileEdit.submit)
        sleep(1)

        try:
            assert "1 Jan 1999" in driver.page_source
        except AssertionError:
            print(f"{WHITE}Year is {RED}NOT LOCATED")
        else:
            print(f"{WHITE}Changed year is {GREEN} LOCATED")

        try:
            assert f_name[0] in driver.page_source
        except AssertionError:
            print(f"{WHITE}Changed name is {RED}NOT LOCATED")
        else:
            print(f"{WHITE}Changed name is {GREEN} LOCATED")

        try:
            assert s_name[0] in driver.page_source
        except AssertionError:
            print(f"{WHITE}Changed surname is {RED}NOT LOCATED")
        else:
            print(f"{WHITE}Changed surname is {GREEN} LOCATED")

        try:
            assert "Changed hobby" in driver.page_source
        except AssertionError:
            print(f"{WHITE}Hobby is {RED}NOT CHANGED")
        else:
            print(f"{WHITE}Hobby is {GREEN} CHANGED")

        try:
            assert "City-17" in driver.page_source
        except AssertionError:
            print(f"{WHITE}City is {RED}NOT CHANGED")
        else:
            print(f"{WHITE}City is {GREEN} CHANGED")
        driver.quit()

    @staticmethod
    def password_change(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("x-test", UserAccount.login)
        action.typewrite("typical_password", UserAccount.password)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        action.clicker(UserAccount.edit_profile)
        sleep(2)
        gui.scroll(-2000)
        action.clicker(UserAccount.Password.change_password_btn)
        sleep(0.5)
        action.typewrite(password(), UserAccount.Password.old_password_field)
        action.typewrite("some_typical_password", UserAccount.Password.new_password_filed)
        action.typewrite("some_typical_password", UserAccount.Password.confirmation_password_field)
        sleep(3)
        action.clicker(UserAccount.Password.submit)
        sleep(3)
        try:
            assert 'Your old password was entered incorrectly. Please enter it again.' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'Your old password was entered incorrectly. Please enter it again.' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'Your old password was entered incorrectly. Please enter it again.' message is "
                  f"{GREEN} LOCATED ")
        sleep(1)
        action.typewrite("typical_password", UserAccount.Password.old_password_field)
        action.typewrite(password(), UserAccount.Password.new_password_filed)
        action.typewrite(password(), UserAccount.Password.confirmation_password_field)
        sleep(0.7)
        action.clicker(UserAccount.Password.submit)
        sleep(0.5)
        try:
            assert 'The two password fields didn’t match.' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'The two password fields didn’t match.' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'The two password fields didn’t match.' message is "
                  f"{GREEN} LOCATED ")
        sleep(1)
        action.typewrite("typical_password", UserAccount.Password.old_password_field)
        action.typewrite("k_ghj01", UserAccount.Password.new_password_filed)
        action.typewrite("k_ghj01", UserAccount.Password.confirmation_password_field)
        sleep(0.7)
        action.clicker(UserAccount.Password.submit)

        try:
            assert 'This password is too short. It must contain at least 8 characters.' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'This password is too short. It must contain at least 8 characters.' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'This password is too short. It must contain at least 8 characters.' message is "
                  f"{GREEN} LOCATED ")

        sleep(1)
        action.typewrite("typical_password", UserAccount.Password.old_password_field)
        action.typewrite("123qwe", UserAccount.Password.new_password_filed)
        action.typewrite("123qwe", UserAccount.Password.confirmation_password_field)
        sleep(0.7)
        action.clicker(UserAccount.Password.submit)

        try:
            assert 'This password is too common' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'This password is too common' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'This password is too common' message is "
                  f"{GREEN} LOCATED ")

        sleep(1)
        action.typewrite("typical_password", UserAccount.Password.old_password_field)
        action.typewrite("5435345345", UserAccount.Password.new_password_filed)
        action.typewrite("5435345345", UserAccount.Password.confirmation_password_field)
        sleep(0.7)
        action.clicker(UserAccount.Password.submit)

        try:
            assert 'This password is entirely numeric' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'This password is entirely numeric' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'This password is entirely numeric' message is "
                  f"{GREEN} LOCATED ")

        sleep(1)
        action.typewrite("typical_password", UserAccount.Password.old_password_field)
        action.typewrite("typical_password", UserAccount.Password.new_password_filed)
        action.typewrite("typical_password", UserAccount.Password.confirmation_password_field)
        sleep(0.7)
        action.clicker(UserAccount.Password.submit)
        sleep(1)

        try:
            assert 'Your password has been successfully changed!' in driver.page_source
        except AssertionError:
            print(f"{WHITE}'Your password has been successfully changed!' message is "
                  f"{RED}NOT LOCATED ")
        else:
            print(f"{WHITE}'Your password has been successfully changed!' message is "
                  f"{GREEN} LOCATED ")
            print(f"{RED} OOPS, I managed to change password to the very same it used to be before password change"
                  f" procedure")
        sleep(1)
        driver.quit()


class PhotoInteraction(Methods, Urls, UserAccount):
    @staticmethod
    def load_photo(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("photo_test", UserAccount.login)
        action.typewrite("typical_password", UserAccount.password)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        action.clicker(UserAccount.Photos.add_photo_btn)
        sleep(2)
        action.clicker(UserAccount.Photos.choose_photo)
        sleep(3)
        gui.typewrite(img1)
        gui.press("enter")
        action.typewrite("Photo test description 1", UserAccount.Photos.img_desc_field)
        action.clicker(UserAccount.Photos.submit)
        sleep(2)

        try:
            assert "Photo test description 1" in driver.page_source
        except AssertionError:
            print(f"{WHITE}Photo test description 1 {RED} NOT LOCATED")
        else:
            print(f"{WHITE}'Photo test description 1' text is {GREEN} LOCATED")

        try:
            check = action.check_exists_by_xpath(UserAccount.Photos.first_photo_file)
            print(f"{WHITE}Check status is: {GREEN}{check}{WHITE}. Photo {GREEN}IS AVAILABLE")

        finally:
            driver.quit()

    @staticmethod
    def delete_photo(browser):
        action = Methods(browser)
        action.open_url(Urls.authorization_link)
        action.typewrite("photo_test", UserAccount.login)
        action.typewrite("typical_password", UserAccount.password)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        action.clicker(UserAccount.Photos.first_photo_file)
        sleep(1)
        action.clicker(UserAccount.Photos.cancel)
        sleep(0.5)
        action.clicker(UserAccount.Photos.first_photo_file)
        action.clicker(UserAccount.Photos.delete)
        assert "Photo test description 1" not in driver.page_source, f"{RED}By some reason I'm not able to delete photo"
        print(f"{GREEN} Looks like photo has just successfully been deleted")
        driver.quit()


class Sorting(Methods, Urls, UserAccount, UserRegistration):

    @staticmethod
    def creation_and_check(browser):
        nick = ''.join([x for x in name_for_email()])
        frst_name = ''.join([x for x in name])
        scnd_name = ''.join([x for x in surname])
        default_date = ''.join([d for d in years_ago(20)])
        city = ''.join([town for town in get_city()])
        action = Methods(browser)
        action.open_url(Urls.registration_page)
        action.typewrite(nick, UserRegistration.username)
        sleep(3)
        password_for_registration = ''.join([x for x in password()])
        action.typewrite(password_for_registration, UserRegistration.password)
        action.typewrite(password_for_registration, UserRegistration.password_confirmation)
        action.typewrite(frst_name, UserRegistration.first_name)
        action.typewrite(scnd_name, UserRegistration.last_name)
        action.typewrite(e_mail(), UserRegistration.email)
        action.typewrite(city, UserRegistration.city)
        action.clear_field(UserRegistration.dob)
        sleep(0.5)
        action.typewrite(default_date, UserRegistration.dob)
        sleep(1)
        gui.scroll(-2000)
        action.clicker(UserRegistration.choose_photo)
        sleep(2)
        gui.typewrite(random_selected_face)
        gui.press("enter")
        sleep(2)
        action.typewrite("Incapsulated", UserRegistration.hobby)
        action.clicker(UserRegistration.submit)
        sleep(4)

        action.clicker(UserAccount.Date.main_page)
        action.clicker(UserAccount.Date.login)
        sleep(2)
        action.typewrite("photo_test", UserAccount.login)
        action.typewrite("typical_password", UserAccount.password)
        sleep(1)
        action.clicker(UserAccount.login_btn)
        sleep(2)
        action.clicker(UserAccount.Date.find_a_date)
        sleep(2)

        action.typewrite(nick, UserAccount.Date.nickname)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by nickname---")
        try:
            assert str(frst_name) in driver.page_source
        except AssertionError:
            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        action.clicker(UserAccount.Date.clear)
        sleep(0.5)
        action.typewrite(frst_name, UserAccount.Date.first_name)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by first name---")
        try:
            assert frst_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:
            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        action.clicker(UserAccount.Date.clear)
        sleep(0.5)
        action.typewrite(scnd_name, UserAccount.Date.last_name)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by second name---")
        try:
            assert frst_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:
            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        action.clicker(UserAccount.Date.clear)
        sleep(0.5)
        action.typewrite(city, UserAccount.Date.city)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by city---")
        try:
            assert frst_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:
            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        action.clicker(UserAccount.Date.clear)
        sleep(0.5)
        action.typewrite(default_date, UserAccount.Date.dob)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by Date of birth---")
        try:
            assert frst_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:
            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        action.clicker(UserAccount.Date.clear)
        sleep(0.5)
        action.typewrite("Incapsulated", UserAccount.Date.hobby)
        sleep(0.5)
        action.clicker(UserAccount.Date.search)
        print(f"{WHITE}---Validation by Hobby---")
        try:
            assert frst_name in driver.page_source
        except AssertionError:

            print(f'{WHITE}{frst_name} is {RED}not located')
        else:
            print(f'{WHITE}{frst_name} is {GREEN} located')

        try:
            assert scnd_name in driver.page_source
        except AssertionError:
            print(f'{WHITE}{scnd_name} is {RED}not located')
        else:
            print(f'{WHITE}{scnd_name} is {GREEN} located')

        try:
            assert city in driver.page_source
        except AssertionError:
            print(f'{WHITE}{city} is {RED}not located')
        else:
            print(f'{WHITE}{city} is {GREEN} located')

        driver.quit()


if __name__ == "__main__":
    start = datetime.now()

    registration_test = ValidRegistrationLinks
    driver = chrome_setup()
    registration_test.start(driver)

    login_url_test = ValidLoginLinks
    driver = chrome_setup()
    login_url_test.start(driver)

    positive_registration = PositiveRegistration
    driver = chrome_setup()
    positive_registration.start(driver)

    negative_registration = NegativeRegistration
    driver = chrome_setup()
    negative_registration.email_is_not_unique(driver)

    negative_registration = NegativeRegistration
    driver = chrome_setup()
    negative_registration.username_taken(driver)

    negative_registration = NegativeRegistration
    driver = chrome_setup()
    negative_registration.both_name_and_email_taken(driver)

    negative_registration = NegativeRegistration
    driver = chrome_setup()
    negative_registration.future(driver)

    negative_registration = NegativeRegistration
    driver = chrome_setup()
    negative_registration.underage(driver)

    positive_auth = Authorization
    driver = chrome_setup()
    positive_auth.authorization_by_login(driver)

    positive_auth = Authorization
    driver = chrome_setup()
    positive_auth.authorization_by_email(driver)

    negative_authorization = Authorization
    driver = chrome_setup()
    negative_authorization.double_authorization(driver)

    negative_authorization = Authorization
    driver = chrome_setup()
    negative_authorization.invalid_auth_by_login(driver)

    negative_authorization = Authorization
    driver = chrome_setup()
    negative_authorization.invalid_auth_by_email(driver)

    logger = UserPage
    driver = chrome_setup()
    logger.logging_out(driver)

    editor = UserPage
    driver = chrome_setup()
    editor.editing(driver)

    password_changer = UserPage
    driver = chrome_setup()
    password_changer.password_change(driver)

    photo_loader = PhotoInteraction
    driver = chrome_setup()
    photo_loader.load_photo(driver)

    photo_deleter = PhotoInteraction
    driver = chrome_setup()
    photo_deleter.delete_photo(driver)

    final_round = Sorting
    driver = chrome_setup()
    final_round.creation_and_check(driver)

    finish = datetime.now()
    estimation = finish - start
    print(f"Elapsed time: {estimation}")





